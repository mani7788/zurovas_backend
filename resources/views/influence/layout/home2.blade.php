@extends('influence.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%">
<script>
    window.toggleFields = function toggleFields(){
        $('#fields').toggle();
        }

</script>
    <div class="container">

        <div class="card">

            <div class="card-header " style="background-color: #F2F7FC;">
             <h3> <strong> Create Post</strong> </h3>
            </div>
            <div class="card-body">
                <div class="container-fluid">
                    <form method="POST" action="{{route('influence.postss') }}" enctype="multipart/form-data">
                        @csrf
                    <div class="row">
                        <div class="col-lg-1">
                            {{--  @if(Storage::disk('public')->exists('property/'.$property->image) && $property->image)
                            <img src="{{Storage::url('property/'.$property->image)}}" alt="{{$property->title}}"  class="avatar-xs rounded-circle mr-2" alt="influencer" style="width: 3.5rem;height: 3rem; ">
                        @endif  --}}
                        <?php  $image=Auth::guard('influence')->user()->user_image; ?>
                            <img src=" {{asset('public/userimage/'.$image)}}" class="avatar-xs rounded-circle mr-2" alt="Shreyu" style="width: 3.5rem;height: 3rem; ">
                         </div>

                        <div class="col-lg-11">

                                <textarea id="my-textarea" class="form-control" name="post_title" rows="5" style="margin-left: 3%; margin-top: 8.39062px; border: none;" placeholder="Title"></textarea>

                        </div>

                        <div class="row" style="display: none" id="fields">
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="image" class=" col-form-label text-md-right">{{ __('Location') }}</label>

                                    <input class="form-control" type="text" name="location" id="location">
                                </div>
                            </div>
                            <div class="col-lg-11">
                                <div class="form-group">
                                    <label for="image" class="col-form-label text-md-right">{{ __('check In') }}</label>

                                    <input class="form-control" type="text" name="chkin" id="chkin" >
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">

                                    <input id="post_image" class="p-0 form-control @error('post_image') is-invalid @enderror" required name="post_image" type="file" accept="image/jpeg, image/png, application/pdf" value="{{ old('post_image') }}" required style="border: none"/>
                                    @error('post_image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                          </div>

                        <div class="col-lg-3">
                            <button type="button" class="btn" id="chkin_location" style="background-color: #F2F7FC; color:black;" onClick="toggleFields()">Check In</button>
                        </div>
                        <div class="col-lg-3">
                            <button type="button" class="btn" style="background-color: #F2F7FC; color:black;">Tag People</button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary btn-lg" style="width: 100%">Post</button>
                        </div>
                    </div>
                     </form>
                </div>
            </div>

          </div>

          @foreach ($influence as $influence)

          <div class="card">
            <div class="card-body">
              <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-1">

                        <img src=" {{asset($influence->influencer_detail->user_image)}}" class="avatar-xs rounded-circle mr-2" alt="Shreyu" style="width: 2.5rem;height: 2rem; margin-top:16%">
                     </div>


                    <div class="col-lg-8">
                        <h5>{{ $influence->influencer_detail->full_name }}</h5>
                        <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Locations.png')}}" class="img-fluid" alt="Shreyu">
                        <span>{{  $influence->location}},{{  $influence->checkin }}</span>
                    </div>

                    <div class="col-lg-2">
                        <form action="{{route('influence.fallllow') }}" method="post">
                            @csrf
                            <input type="hidden" name="customer_id" value="{{ $influence->influencer_detail->id }}">
                            <input type="hidden" name="user_id" value="{{ Auth::guard('influence')->user()->id }}">
                            <button type="submit" class="btn" style="background-color: #F2F7FC; color:black;">Follow</button>

                        </form>
                    </div>
                    {{--  <div class="col-lg-1">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" style="background: none;
                            color: black;
                            border: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Dot_menu.png')}}" class="image-fluid ml-3" alt="Shreyu" >
                              </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#">Add links</a>
                            </div>
                          </div>
                        </div>  --}}
                </div>
              </div>

              <p style="margin-top:3%">{{ $influence->post_title  }}</p>
              <img src="{{asset('public/postImages/'.$influence->post_image)}}" class="img-fluid" width="100%" alt="Shreyu" >
              <div class="container-fluid">
                  <div class="row">
                    <div class="col-lg-4">
                       <div class="row" style="border-right: 1px solid darkgray; margin-top: 8%;">

                            <div class="col-lg-4">
                            <form method="post" action="{{route('save_likes')}}">
                                @csrf
                            <input class="form-control" type="hidden" name="post_id" value={{$influence->id}}>
                            <button type="submit" style="border: none; background-color: white;" >
                                <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Like.png')}}" class="img-fluid" alt="Shreyu" style="margin-left: 62%;">
                            </button>
                            </form>
                            </div>
                            <div class="col-lg-8">
                                <p>Likes<br>{{$influence->totallikes($influence->id)}}</p>
                            </div>


                       </div>
                    </div>
                     <div class="col-lg-4">
                        <div class="row"  style="    border-right: 1px solid darkgray;
                        margin-top: 8%;">
                            <div class="col-lg-4">

                                <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Comments.png')}}" class="img-fluid" alt="Shreyu" style="margin-left: 62%;">
                            </div>

                            <div class="col-lg-8">
                                <p>Comments<br>{{$influence->totalcomments($influence->id)}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                       <div class="row"  style=" margin-top: 8%;">
                        <div class="col-lg-4">

                            <button data-toggle="modal" data-target="#exampleModal{{ $influence->id }}" style="border: none; background-color: white;">
                            <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Share.png')}}" class="img-fluid" alt="Shreyu" style="margin-left: 62%;">
                        </div>
                         </button>
                        <div class="col-lg-8">
                            <p>Shares<br>{{$influence->totalshares($influence->id)}}</p>
                        </div>
                        <div class="modal fade" id="exampleModal{{ $influence->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Share post</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('save_shares')}}">
                                        @csrf
                                    <input class="form-control" type="hidden" name="post_id" value={{$influence->id}}>
                                    <div class="form-group">
                                        <label for="image" class=" col-form-label text-md-right">{{ __('Share Message') }}</label>

                                      <input type="text" class="form-control" id="" name="message" value="">
                                    </div>
                                    <button name="" id="" class="btn btn-primary" type="submit">Share Now</button>

                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                       </div>

                    </div>
                    <div class="col-lg-8">
                     <h6><strong>
                         <ul>
                             <li>Latest Comment: {{$influence->latest_comment($influence->id)  }}</li>
                         </ul>

                    </strong> </h6>

                    </div>
                    <div class="col-lg-8 mt-3">
                    <form method="post" action="{{route('save_comments')}}">
                        @csrf

                        <div class="form-group">
                            <input class="form-control" type="hidden" name="post_id" value={{$influence->id}}>
                            <input class="form-control" type="text" name="comment" id="location">
                        </div>
                    </div>
                    <div class="col-lg-4 mt-3" >
                   <button name="" id="" class="btn btn-primary" type="submit">Add Comment</button>
                    </div>
                    </form>
                  </div>
              </div>
            </div>
          </div>

          @endforeach
    </div>

</main>
@endsection
<!-- end::main content -->
