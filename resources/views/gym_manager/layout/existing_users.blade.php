@extends('gym_manager.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;">

        <div class="container">
            <div class="row p-lg-3 mt-lg-5" style="background-color: #ffffff">
                <div class="col-lg-6 text-center">
                    <h5> <a href="http://">ADD NEW USER</a> </h5>

                </div>
                <div class="col-lg-6 text-center">
                    <h5> <a href="http://">EXISTING USERS</a></h5>

                </div>
                <div class="col-lg-12 ">
                    <hr>
                </div>
                
            </div>
            
            <div class="row p-lg-3" style="background-color: #ffffff">
                <div class="col-lg-12">
                    <h5>Total Users 234</h5>
                </div>
                <div class=" container pl-lg-3 pr-lg-3" style="background-color: #ffffff">
                    <div class="row p-lg-5" >

                        <div class="col-lg-2">
                            <img src="{{asset('public\assets\images\users\avatar-7.png')}}" class="image-fluid rounded-circle mr-2 mt-3" alt="Shreyu" style="    width: 100%;">
                        </div>
                        <div class="col-lg-6">
                            <h5>Catilyn Thompson</h5>
                            <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Locations.png')}}" class="img-fluid" alt="Shreyu">
                            <span>Fithbuz SC,vancour</span>
                            <br>
                            <p class="float-left pt-3">7:30 pm</p>
                            <p class="float-right pt-3">11/11/2019</p>
                        </div>
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-2 " >
                            <div class="dropdown ">
                                <button class="btn btn-secondary dropdown-toggle" style="background: none;
                                     color: black;
                                     border: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                     <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Dot_menu.png')}}" class="image-fluid ml-5 float-right" alt="Shreyu" >
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                     <a class="dropdown-item" href="#">Add links</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class=" container pl-lg-3 pr-lg-3 mt-3">
                <div class="row p-lg-5" style="background-color: #ffffff">

                    <div class="col-lg-2">
                        <img src="{{asset('public\assets\images\users\avatar-7.png')}}" class="image-fluid rounded-circle mr-2 mt-3" alt="Shreyu" style="    width: 100%;">
                    </div>
                    <div class="col-lg-6">
                        <h5>Catilyn Thompson</h5>
                        <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Locations.png')}}" class="img-fluid" alt="Shreyu">
                        <span>Fithbuz SC,vancour</span>
                        <br>
                        <p class="float-left pt-3">7:30 pm</p>
                        <p class="float-right pt-3">11/11/2019</p>
                    </div>
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-2 " >
                        <div class="dropdown ">
                            <button class="btn btn-secondary dropdown-toggle" style="background: none;
                                 color: black;
                                 border: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Dot_menu.png')}}" class="image-fluid ml-5 float-right" alt="Shreyu" >
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                 <a class="dropdown-item" href="#">Add links</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </div>
    

</main>
@endsection

<!-- end::main content -->
