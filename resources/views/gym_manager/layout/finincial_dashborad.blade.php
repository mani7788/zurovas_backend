@extends('gym_manager.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;background-color: #ffffff;">

   <div class="container">
      <div class="row p-lg-3">
         <div class="col-lg-12">
            <h3>Finincial Dashborad</h3>
            <div class="row">
                <div class="col-lg-5">
                    <input id="datepicker" width="270" style="padding: 9px"/>
                    <script>
                        $('#datepicker').datepicker({
                            uiLibrary: 'bootstrap'
                        });
                    </script>
                   
                    <span>
                        <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Calendar.png')}}" class="image-fluid" alt="Shreyu" style=" background: white;z-index: 1;position: relative;float: right;margin-right: -27px; margin-top: -32px; width: 9%;" >
                    </span>
                </div>

                <div class="col-lg-6 ml-5">
                    <input id="datepicker1" width="270" style="padding: 9px"/>
                    <span>
                        <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Calendar.png')}}" class="image-fluid" alt="Shreyu" style=" background: white;z-index: 1;position: relative;float: right;margin-right: 21px;margin-top: -34px;width: 8%;
                    }" >
                    </span>
                    <script>
                        $('#datepicker1').datepicker({
                            uiLibrary: 'bootstrap'
                        });
                    </script>
                </div>
            </div>

         </div>
      </div>
      <div class="row ">
         <div class="col-lg-5 m-4" style="background-color: #EEF5FF;">
            <div class="row pt-4 pb-4">
               <div class="col-lg-4 ">
                  <img src="{{asset('public\assets\ZURVOS_ASSETS\IMAGES\WEB\Icon_6.png')}}" class="avatar-md rounded-circle mt-3" width="100%" alt="Shreyu" style="background:white" >
               </div>
               <div class="col-lg-8">
                  <h3><strong>$170.00</strong></h3>
                  <h5>Earned this month</h5>
               </div>
            </div>
         </div>
         <div class="col-lg-5 m-4" style="background-color: #EEF5FF;">
            <div class="col-lg-12">
               <div class="row pt-4 pb-4">
                  <div class="col-lg-4">
                     <img src="{{asset('public\assets\ZURVOS_ASSETS\IMAGES\WEB\Icon_7.png')}}" class="avatar-md rounded-circle mt-3" width="100%" alt="Shreyu" style="background:white" >
                  </div>
                  <div class="col-lg-8">
                     <h3><strong>$20,170.00</strong></h3>
                     <h5>Total Earned </h5>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row mt-lg-4">
         <div class="col-lg-12">
            <h4>Recent Transactions</h4>
         </div>
      </div>
      <div class="row p-5 ">

         <div class="col-lg-4">
             <img src="{{asset('public\assets\images\users\avatar-7.png')}}" class="avatar-sm rounded-circle" alt="Shreyu" style="margin-left: -8px; margin-top: 4px;">
            <span style="float: right; margin-right: 3%;">
               <p style="margin-bottom: 1%;">Catilyn Thompson</p>
               <p >Fithbuz</p>
            </span>
            </div>
         <div class="col-lg-4 text-center">
            <p style="margin-bottom: 1%;">22/02/2019</p>
            <p >#1245875</p>
         </div>
         <div class="col-lg-3 ">
            <a href="http://"><h3>$23.00</h3></a>
         </div>
         <div class="col-lg-1 ">
            <div class="dropdown ">
               <button class="btn btn-secondary dropdown-toggle" style="background: none;
                    color: black;
                    border: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Dot_menu.png')}}" class="image-fluid  float-left" alt="Shreyu" >
               </button>
               <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Add links</a>
               </div>
           </div>
         </div>
     </div>
     <div class="row pl-5 pr-5 pt-2">

      <div class="col-lg-4">
          <img src="{{asset('public\assets\images\users\avatar-7.png')}}" class="avatar-sm rounded-circle" alt="Shreyu" style="margin-left: -8px; margin-top: 4px;">
         <span style="float: right; margin-right: 3%;">
            <p style="margin-bottom: 1%;">Catilyn Thompson</p>
            <p >Fithbuz</p>
         </span>
         </div>
      <div class="col-lg-4 text-center">
         <p style="margin-bottom: 1%;">22/02/2019</p>
         <p >#1245875</p>
      </div>
      <div class="col-lg-3 ">
         <a href="http://"><h3>$23.00</h3></a>
      </div>
      <div class="col-lg-1 ">
         <div class="dropdown ">
            <button class="btn btn-secondary dropdown-toggle" style="background: none;
                 color: black;
                 border: none;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Dot_menu.png')}}" class="image-fluid  float-left" alt="Shreyu" >
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                 <a class="dropdown-item" href="#">Add links</a>
            </div>
        </div>
      </div>
  </div>
   </div>

</main>
@endsection

<!-- end::main content -->
