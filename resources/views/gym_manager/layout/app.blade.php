<!DOCTYPE html>
<html lang="en">
<head>
	<title>Zurvos</title>

	   @include('gym_manager.layout.header')

</head>
<body >

    <div id="wrapper">

    	@include('gym_manager.layout.topbar')
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    @include('gym_manager.layout.navbar')
                </div>
                <div class="col-lg-6">
                    @section('main-content')
			        @show
                </div>
                <div class="col-lg-3 ">
                    @include('gym_manager.layout.rightsidebar')
                </div>
            </div>
        </div>
	    @include('gym_manager.layout.footer')
    </div>
</body>
</html>
