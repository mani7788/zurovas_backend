@extends('gym_manager.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;background-color:  #EEF5FF;">
    <div class="container">
        <div class="row m-5">

        </div>
      <div class="row p-lg-3" style="background-color:  #FFFFFF;margin: 24px;">
            <div class="col-lg-12 text-center">
                    <h3>Location Detail</h3>
            </div>
        <div class="col-lg-12">

            <div class="col-lg-12 mt-lg-5">
                <label for=""> Location name</label>
                <input class="form-control" type="text" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Zip code</label>
                <input class="form-control" type="text" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Cost per session</label>
                <input class="form-control" type="text" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Phone number</label>
                <input class="form-control" type="text" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5 mb-lg-5">
                <a name="" id="" class="btn btn-primary btn-lg" href="#" role="button" style="width: 100%">Send</a>
            </div>
         </div>
   </div>

</main>
@endsection

<!-- end::main content -->
