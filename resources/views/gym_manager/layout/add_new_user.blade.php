@extends('gym_manager.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;background-color:  #EEF5FF;">
    <div class="container">
        <div class="row m-5">

        </div>

      <div class="row p-lg-3" style="background-color:  #FFFFFF;margin: 24px;">
          <div class="col-lg-12">
            <div class="row p-lg-3">
                <div class="col-lg-6 text-center">
                    <h5> <a href="http://">ADD NEW USER</a> </h5>

                </div>
                <div class="col-lg-6 text-center">
                    <h5> <a href="http://">EXISTING USERS</a></h5>

                </div>
                <div class="col-lg-12 ">
                    <hr>
                </div>

            </div>
          </div>
            <div class="col-lg-12 text-center">
                    <h3>Add New Employee </h3>
            </div>
        <div class="col-lg-12">

            <div class="col-lg-12 mt-lg-5">
                <label for=""> User Name</label>
                <input class="form-control" type="text" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Email Address</label>
                <input class="form-control" type="email" name="" placeholder="">
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Password</label>
                <input class="form-control" type="text" name="" placeholder="">
                <span>
                    <img src="{{asset('public/assets/ZURVOS_ASSETS/ICONS/eye.png')}}" class="image-fluid rounded-circle float-right" alt="Shreyu" style=" z-index:1;margin-top: -6%;margin-right: 3%;">
                </span>
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for="">Confirm Password</label>
                <input class="form-control" type="text" name="" placeholder="">
                <span>
                    <img src="{{asset('public/assets/ZURVOS_ASSETS/ICONS/eye.png')}}" class="image-fluid rounded-circle float-right" alt="Shreyu" style=" z-index:1;margin-top: -6%;margin-right: 3%;">
                </span>
            </div>
            <div class="col-lg-12 mt-lg-5 mb-lg-5">
                <a name="" id="" class="btn btn-primary btn-lg" href="#" role="button" style="width: 100%">Create Account</a>
            </div>
         </div>
   </div>

</main>
@endsection

<!-- end::main content -->
