@extends('influence.layout.app')

@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;background-color: #fff;">

   <div class="container" style="padding: 0px 0px 20px 0px;">
      <img src="{{asset('public\assets\ZURVOS_ASSETS\IMAGES\WEB\Timeline_image_2.png')}}" class="image-fluid" alt="Shreyu" width="100%" >
      <div class="row">
         <div class="col-lg-12 text-center">
            <img src="{{asset('public/assets/images/users/avatar-7.png')}}" class=" rounded-circle" alt="Shreyu" width="15% " style="z-index: 1; margin-top: -9%;" />
            <img src="{{asset('public\assets\ZURVOS_ASSETS\RAW_IMAGES\camera.png')}}" class=" rounded-circle" alt="Shreyu" width="5% " style="z-index: 2; margin-left: -5%; background-color: #ffffff;" />
            <h5>User Name</h5>

         </div>

      </div>

   </div>
   <div class="container pl-lg-5 pr-lg-5">
        <div class="row">
            <div class="col-lg-12 ">
                <h3>Change Password</h3>
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for=""> Password</label>
                <input class="form-control" type="text" name="" placeholder="">
                <span>
                    <img src="{{asset('public/assets/ZURVOS_ASSETS/ICONS/eye.png')}}" class="image-fluid rounded-circle float-right" alt="Shreyu" style=" z-index:1;margin-top: -5%;margin-right: 3%;">
                </span>
            </div>
            <div class="col-lg-12 mt-lg-5">
                <label for="">Confirm Password</label>
                <input class="form-control" type="text" name="" placeholder="">
                <span>
                    <img src="{{asset('public/assets/ZURVOS_ASSETS/ICONS/eye.png')}}" class="image-fluid rounded-circle float-right" alt="Shreyu" style=" z-index:1;margin-top: -5%;margin-right: 3%;">
                </span>
            </div>
            <div class="col-lg-12 mt-lg-5 mb-lg-5">
                <a name="" id="" class="btn btn-primary btn-lg" href="#" role="button" style="width: 100%">Create Account</a>
            </div>
        
        </div>
   </div>
  
</main>

@endsection


<!-- end::main content -->
