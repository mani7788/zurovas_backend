@extends('gym_manager.layout.app')


@section('main-content')
<!-- begin::main content -->
<main class="main-content" style="margin-top: 13%;background-color: #fff;">

   <div class="container" style="padding: 0px 0px 20px 0px;">
      <img src="{{asset('public\assets\ZURVOS_ASSETS\IMAGES\WEB\Timeline_image_2.png')}}" class="image-fluid" alt="Shreyu" width="100%" >
      <a name="" id="" class="btn btn-primary" href="#" role="button" style="    z-index: 4;float: right;margin-top: -25%;margin-right: 5%;position: relative; background-color: #7D7B78;border: none;">Edit Profile</a>
      <div class="row">
         <div class="col-lg-12 text-center">
            <img src="{{asset('public/assets/images/users/avatar-7.png')}}" class=" rounded-circle" alt="Shreyu" width="15% " style="z-index: 1; margin-top: -9%;" />
            <img src="{{asset('public\assets\ZURVOS_ASSETS\RAW_IMAGES\camera.png')}}" class=" rounded-circle" alt="Shreyu" width="5% " style="z-index: 2; margin-left: -5%; background-color: #ffffff;" />
            <h5>Catilyn Thompson</h5>

         </div>
         <div class="col-lg-12 mt-4">
            <div class="row">
               <div class="col-lg-4 text-right">
                  <a href="http://">23</a><span> Posts</span>
               </div>
               <div class="col-lg-4 text-center">
                  <a href="http://">203</a><span> Followers</span>
               </div>
               <div class="col-lg-4 text-left">
                  <a href="http://">22</a><span> Following</span>
               </div>
            </div>
         </div>
      </div>
   </div>

</main>
<main class="main-content" style="margin-top: 4%;background-color: #fff;">

   <div class="container" style="padding: 40px 10px 20px 10px;">
         <div class="row">
            <div class="col-lg-12">
                  <h3>Bio</h3>
                  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate vitae voluptatum voluptatem magni laboriosam atque vel cum commodi. Iure distinctio ipsum voluptates porro, et blanditiis at neque placeat aut doloribus!</p>
            </div>
            <div class="col-lg-12 mt-5">
                <button type="submit" class="btn btn-primary" style="margin-left: 20px; margin-top: 20px; width:92%;">Add Location</button>

            </div>
            <div class="col-lg-12 mt-5">
                <div class="container-fluid mt-3">
                    <div class="row">
                        <div class="col-lg-3 ml-3">
                            <div style="background-color: #BDD4F6;border-radius: 25px; width: 42%; height: 61%; text-align:center; color:#1A69DF;margin: 17px 0px 0px 24px;">
                               <p style="font-size: 30px ">A</p>
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <h5> York Street Crossit</h5>
                            <img src="{{asset('public\assets\ZURVOS_ASSETS\ICONS\Locations.png')}}" class="img-fluid" alt="Shreyu">
                             <span>Fithbuz .3Miles 5min<br></span>
                             <a href="http://" >Cost:$3.50/Workout</a>
                        </div>
                    </div>
                </div>
            </div>
         </div>
   </div>

</main>
@endsection


<!-- end::main content -->
