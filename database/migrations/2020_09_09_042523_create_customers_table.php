<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('user_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->text('street_address')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('tiktok_link')->nullable();
            $table->integer('charges')->nullable();
            $table->string('phone_number')->unique()->nullable();
            $table->string('paypalemail')->unique()->nullable();
            $table->string('paypalemail_document')->unique()->nullable();
            $table->string('location')->nullable();
            $table->string('bio')->nullable();
            $table->string('t_shirt_size')->nullable();
            $table->string('affiliated_link')->nullable();
            $table->enum('customer_type', ['customers', 'influence']);
            $table->enum('status', ['inactive', 'active']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
