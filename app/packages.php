<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class packages extends Model
{
    public function packages_user_packages()
    {
        return $this->hasMany(packages_user::class,'package_id');
    }
}
