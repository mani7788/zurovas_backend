<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AllTransactionCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    	
    	return [

            'id' => $this->id,
            'amount'=>$this->amount,

            'currency' => $this->currency ==null ? '0' : $this->currency,
            'description' => $this->description ==null ? '0' : $this->description,

            'status' => $this->status ==null ? '0' : $this->status,
       
            'charge_id' => $this->charge_id ==null ? '0' : $this->charge_id,
            'order_for' => $this->order_for ==null ? '0' : $this->order_for,
            'video_order_id' => $this->video_order_id ==null ? '0' : $this->video_order_id,
            'pkg_order_id' => $this->pkg_order_id ==null ? '0' : $this->pkg_order_id,
            'pro_order_id' => $this->pro_order_id ==null ? '0' : $this->pro_order_id,
            'user_name' => $this->user_name ==null ? '0' : $this->user_name,
            'email' => $this->email ==null ? '0' : $this->email,
            'phone' => $this->phone ==null ? '0' : $this->phone,
            't_type' => $this->t_type ==null ? '0' : $this->t_type,
        ];
    }

}