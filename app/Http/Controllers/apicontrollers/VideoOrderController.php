<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\VideoOrder;
use App\Customer;
use App\models\Tutorial;
use App\Http\Resources\AllOrderVideoCollection;
use DB;
use Stripe;
use App\models\Transaction;
use App\Payment_method;
use Validator;
class VideoOrderController extends Controller
{
    public function store(Request $request)
    {
        $input = array(
            'tutorial_id' => $request->tutorial_id,
            'user_id' => $request->user_id
        );
        $rules = array(
            'tutorial_id' => 'required',
            'user_id' => 'required'
        );
        $validator = Validator::make($input, $rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }
        $user=Customer::find($request->user_id);
        if ($user == "" && $user == null) {
            return response()->json(["message" => "User doest not exist", "status" => 0]);
        }
        $method = Payment_method::where(['user_id' => $request->user_id])->get()->first();
        if(!($method->count() > 0)){
            return response()->json(["message" => "Sorry! Payment method is not existing", "status" => 0]);
        }
        $products = Tutorial::where('id', $request->tutorial_id)->get()->first();
        if ($products->type == 1) {
            if ($method->avalable_amount >=   $products->course_price ) {
                $method->avalable_amount=$method->avalable_amount - $products->course_price;
                return $method;
                if ($method->save()) {
                    $product=Tutorial::find($request->tutorial_id);
                    $order=new VideoOrder();
                    $order->user_id=$user->id;
                    $order->tutorial_id=$product->id;
                    $order->latitude=$request->latitude;
                    $order->longitude=$request->longitude;
                    $order->address=$request->address;
                    $order->save();
                    $payment=new Transaction();
                    $payment->charge_id="video_order".time().$request->user_id.$request->tutorial_id.$order->id;
                    $payment->amount=$products->course_price;
                    $payment->currency="usd";
                    $payment->description="payment from deposit";
                    $payment->status="succeeded";
                    $payment->order_for="video Purchase Amount";
                    $payment->video_order_id=$order->id;
                    $payment->user_name=$user->full_name;
                    $payment->email=$user->email;
                    $payment->phone=$user->phone_no;
                    $payment->t_type=1;
                    $result=$payment->save();
                    return response()->json(["message" => "Successfully purchased product", "status" => true,'transsaction' => $payment]);
                }
            }
            try {
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                 $response = \Stripe\Token::create(array(
                   "card" => array(
                     "number"    => $method->card_number,
                     "exp_month" => \Carbon\Carbon::parse($method->expire_date)->month,
                     "exp_year"  => \Carbon\Carbon::parse($method->expire_date)->year,
                     "cvc"       => $method->cvv_code,
                     "name"      => $method->card_holder_number
                 )));
            } catch (\Exception $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];
                return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
             }
            
             $token = $response->id;
             $ss = false;
             if ($method->avalable_amount > 0) {
                 $ss = true;
             }
             try {
                 $stripe = Stripe\Charge::create ([
                         "amount" => ($products->course_price-$method->avalable_amount)*100,
                         "currency" => "usd",
                         "source" => $token,
                         "description" => "Client from Card payment." 
                 ]);
             } catch (\Exception $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];
                return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
             }
            if ($ss = true) {
                $method->avalable_amount=0;
                $method->save();
            }
        }

        $product=Tutorial::find($request->tutorial_id);
        $order=new VideoOrder();
        $order->user_id=$user->id;
        $order->tutorial_id=$product->id;
        $order->latitude=$request->latitude;
        $order->longitude=$request->longitude;
        $order->address=$request->address;
        $order->save();
        if ($products->type == 1) {
            $payment=new Transaction();
            $payment->charge_id="video_order".time().$request->user_id.$request->tutorial_id.$order->id;
            $payment->amount=$stripe->amount/100;
            $payment->currency=$stripe->currency;
            $payment->description=$stripe->description;
            $payment->status=$stripe->status;
            $payment->order_for="video order Amount";
            $payment->video_order_id=$order->id;
            $payment->user_name=$user->full_name;
            $payment->email=$user->email;
            $payment->phone=$user->phone_no;
            $payment->t_type=1;
            $result=$payment->save();
            if($result){

                     return response(['message' => 'Order Successfully Place',"status" => true,'transsaction' => $payment]);

                 }else{
                     
                    return response(['message' => 'Order Not Place','status' =>'error']);
                 }
        }
        return response(['message' => 'Order Successfully Place',"status" => true]);
    }
    
    public function allorder()
    {
        $order=DB::table('video_orders')
        ->join('tutorials','tutorials.id','video_orders.tutorial_id')
        ->join('customers','customers.id','video_orders.user_id')
        ->select('video_orders.*','tutorials.course_name','tutorials.course_price','tutorials.category','tutorials.course_videos','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

    	         return  AllOrderVideoCollection::collection($order);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Found','status' =>'error']);
    	     }
    }
    
    public function pendingorder()
    {
        $order=DB::table('video_orders')->where('video_orders.status','Pending')
        ->join('tutorials','tutorials.id','video_orders.tutorial_id')
        ->join('customers','customers.id','video_orders.user_id')
        ->select('video_orders.*','tutorials.course_name','tutorials.course_price','tutorials.category','tutorials.course_videos','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

    	         return  AllOrderVideoCollection::collection($order);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Found','status' =>'error']);
    	     }
    }
    
    public function changeorder($id,$status)
    {
        $state = "pending";
        if ($status == 1) {
            $state = "Completed";
        }elseif ($status == 2) {
            $state = "Cancelled";
        }
        $order=VideoOrder::find($id);
        $order->status=$state;
        $order->save();
        if($order){

                 return response(['message' => 'Order Successfully Updated as '.$state,'status' =>'success']);

             }else{
                 
                return response(['message' => 'Order Not Place','status' =>'error']);
             }
    }
    public function ProcessingOrder()
    {
        $order=DB::table('video_orders')->where('video_orders.status','Processing')
        ->join('tutorials','tutorials.id','video_orders.tutorial_id')
        ->join('customers','customers.id','video_orders.user_id')
        ->select('video_orders.*','tutorials.course_name','tutorials.course_price','tutorials.category','tutorials.course_videos','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

                 return  AllOrderVideoCollection::collection($order);

             }else{
                 
                return response(['message' => 'Order Not Found','status' =>'error']);
             }
    }

    public function changeorderProcessing($id)
    {
        $order=VideoOrder::find($id);
        $order->status="Processing";
        $order->save();
        if($order){

                 return response(['message' => 'Order Successfully Updated','status' =>'success']);

             }else{
                 
                return response(['message' => 'Order Not Place','status' =>'error']);
             }
    }

}
