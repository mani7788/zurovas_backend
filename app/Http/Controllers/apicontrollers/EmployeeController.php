<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Admin;
use App\models\ContactGym;
use App\Http\Resources\PartnersCollection;

class EmployeeController extends Controller
{
    public function addemployee(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6',
            'gym_id' => 'required',
        ]);
        $countGym = Admin::where('id', $request->gym_id)->get()->count();
        if ($countGym > 0) {
            $employee=new Admin();
            $employee->name=$request->name;
            $employee->email=$request->email;
            $employee->role="employee";
            $employee->password=bcrypt($request->password);
            $employee->gym_id=$request->gym_id;
            $result=$employee->save();
            if ($result) {

                 return response(['message' => 'Employee Added Successfully','status' =>'success']);
                
                 }else{

                return response(['message' => 'Record not created','status' =>'error']);
                
               }
        }
        else {
            return response()->json(["message" => "no gym find against this gym id"]);
        }
        
        
    }

    public function editemployee(Request $request,$id)
    {
        $employee=Admin::find($id);
        $employee->name=$request->name;
        $employee->email=$request->email;
        $employee->password=bcrypt($request->password);
        $employee->gym_id=$request->gym_id;
        $result=$employee->save();
        if ($employee) {

             return response(['message' => 'Employee Update Successfully','status' =>'success']);
            
             }else{

            return response(['message' => 'Record not Update','status' =>'error']);
            
           }
    }

    public function show()
    {
        $employee=Admin::where('role', 'employee')->get();
        if (!empty($employee)) {


            return  PartnersCollection::collection($employee);

        }else{
            
           return response(['message' => 'Employee Not Found','status' =>'error']);
        }
    }

    function contact_support(Request $request) {
        $request->validate([
            'emp_id' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $check = Admin::where(['id' => $request->emp_id, 'role' => 'employee'])->get();
        if($check->count() > 0){
            $contact = new ContactGym();
            $contact->emp_id = $request->emp_id;
            $contact->subject = $request->subject;  
            $contact->user_message = $request->message; 
            if ($contact->save()) {
                 return response()->json(["message" => "Submitted successfully mesaage", "status" => 1]);
             } 
            
        }
        else{
            return response()->json(["message" => "no employee found", "status" => 0]);
        }
        return 123;
    }
}
