<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\chat;
use App\models\BuddyWorkout;
use DB;

class ChatController extends Controller
{
    function sendMessage(Request $request) {
    	$request->validate([
    		'sender_id' => 'required',
    		'receiver_id' => 'required',
    		'message' => 'required',
    	]);
    	$chat = new chat();
    	$chat->sender_id = $request->sender_id;
    	$chat->receiver_id = $request->receiver_id;
    	$chat->message = $request->message;
    	if ($chat->save()) {
    		return response()->json(["message" => "message is sent", "data" => $chat]);
    	}
    }

    function getMessages($buddy_id, $user_id){
    	// //Create an array
    	// $groups = [];

    	// //Retrieve the list of cat_id's in use.
    	// $cats = DB::table('chats')->where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->distinct()->select('receiver_id')->get();

    	// //for each cat_id in use, find the products associated and then add a collection of those products to the relevant array element
    	// foreach($cats as $cat){
    	//     $groups[$cat->receiver_id] = array(
    	//     	"buddy" => DB::table("customers")->where('id', $cat->receiver_id)->get()->first(),
    	//     	"messages" => DB::table('chats')->where('chats.receiver_id', $cat->receiver_id)->where('sender_id',$user_id)->join('customers','customers.id', 'chats.receiver_id')->select('chats.*','customers.full_name','customers.email')->get()
    	//     ); 
    	// }
        $msg = array(
            "message" => DB::table('chats')->where(['sender_id' => $user_id, 'receiver_id'=> $buddy_id])->orWhere(['sender_id'=> $buddy_id, 'receiver_id'=> $user_id])->get(),
        );
        
    	return response()->json($msg);
    }

    function getMessagesBuddies($user_id){
        $msg = [];
        $data = BuddyWorkout::where('buddy_workouts.customer_id',$user_id)
        ->join('customers','customers.id','buddy_workouts.buddy_id')
        ->select('buddy_workouts.buddy_id as buddy_id','customers.full_name')->distinct()
        ->get();
        foreach ($data as $key => $value) {
            $msg = array(
                "buddy" => $value,
                "message" => DB::table('chats')->where('sender_id',$user_id)->orWhere('receiver_id',$user_id)->latest()->first()
            );
        }
        return response()->json($msg);
    }
}
