<?php

namespace App\Http\Controllers\apicontrollers;


use App\Http\Controllers\Controller;
use App\models\Location;
use Illuminate\Http\Request;
use App\Http\Resources\LocationResource;
use App\Admin;

class LocationController extends Controller
{
    public function location($id)
    {
    	$location=Location::where('gym_id',$id)->get();
    	if ($location->count() >0) {


             return  LocationResource::collection($location);

         }else{
             
            return response(['message' => 'Location Not Found','status' =>'error']);
         }
    	
    }

    public function store(Request $request)
    {
        $adminCount = Admin::where(['id'=> $request->gym_id, 'role' => 'gym_manager'])->get();
        $request->validate([
            'gym_id' => 'required|unique:locations'
        ]);
        if ($adminCount->count() > 0) {
                $location=new Location();
                $location->gym_id=$request->gym_id;
                $location->location=$request->location;
                $location->zipcode=$request->zipcode;
                $location->address=$request->address;
                $location->phone=$request->phone;
                $result=$location->save();
                if ($result) {

                     return response(['message' => 'Location added','status' =>'success']);
                    
                     }else{

                    return response(['message' => 'Your Info Not added','status' =>'error']);
                    
                   }
        }
        else {
            return response()->json(["message" => "No gym found", "status" => 0]);
        }
    	
    }
}
