<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gym;
use App\Partner;
use App\Admin;
use App\Employee;
class AdminController extends Controller
{
    public function getGymActivities(){
      for ($i=0; $i < 12; $i++) { 
        $ds = \Carbon\Carbon::now()->startOfMonth()->subMonth($i);
        $de = \Carbon\Carbon::now()->startOfMonth()->subMonth($i-1);
        if ($i == 0) {
          $users = Admin::where('created_at', '>=', $ds)->get()->count();
          $monthWiseYear[] = ['id' => $i+1, 'month' => \Carbon\Carbon::parse($ds)->format('m'), "gyms" => $users];
        }
        else {
          $users = Admin::where('created_at', '>=', $ds)->where('created_at', '<=', $de)->get()->count();
          $monthWiseYear[] = ['id' => $i+1, 'month' => \Carbon\Carbon::parse($ds)->format('m'), "gyms" => $users];
        }
      }
      $dateStart = \Carbon\Carbon::now()->subDays(30);
      $newUsers = Admin::where('created_at', '>=', $dateStart)->get()->count();
      $activeUsers = 2;
      $planSubscribed = 4;
      //one month / 30 days
      $total = Admin::get()->count();
      return response()->json(['total_gyms' => $total,'new_gyms' => $newUsers, 'inactive_gyms' => $activeUsers, 'monthWiseYearlyGyms' => $monthWiseYear]);
    }
    public function adminlogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $admin = Admin::where('email', '=', $request->input('email'))->first();
        if(!empty($admin))
        {
            if (\Hash::check($request->password, $admin->password)) { 
            return response(['id'=>$admin->id,'name'=>$admin->name,'email'=>$admin->email,'type'=>$admin->role,'message' => 'Login Successfull','status' =>'success']);            
            }

        }    
        return response(['message' => 'No user found agains these credentails','status' =>0]);
    }
}
