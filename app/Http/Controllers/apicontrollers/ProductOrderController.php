<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\ProductOrder;
use App\Customer;
use App\models\Product;
use App\Http\Resources\AllOrderProductsCollection;
use App\Http\Resources\AllOrderVideoCollection;
use App\Http\Resources\AllTransactionCollection;
use DB;
use Stripe;
use App\models\Transaction;
use App\models\GymNotification;
use App\Payment_method;
use Validator;
use App\Http\Resources\gymnotificationcollection;
class ProductOrderController extends Controller
{
    public function video_all_status_order()
    {
        $order=DB::table('product_orders')
        ->join('products','products.id','product_orders.product_id')
        ->join('customers','customers.id','product_orders.user_id')
        ->select('product_orders.*','products.product_type','products.product_name','products.product_description','products.product_price','products.product_image','customers.full_name','customers.email','customers.phone_number')
        ->get();
        $v_order=DB::table('video_orders')
        ->join('tutorials','tutorials.id','video_orders.tutorial_id')
        ->join('customers','customers.id','video_orders.user_id')
        ->select('video_orders.*','tutorials.course_name','tutorials.course_price','tutorials.category','tutorials.course_videos','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0 || $v_order->count() > 0){
             $arr = array(
                    'products_orders' => AllOrderProductsCollection::collection($order),
                    'videos_orders' => AllOrderVideoCollection::collection($v_order)
                 );
                 return   response()->json($arr, 200);

             }else{
                 
                return response(['message' => 'Order Not Found','status' =>'error']);
             }
    }
    public function V_proallorder($id)
    {
        $order=DB::table('product_orders')->where('product_orders.user_id',$id)
        ->join('products','products.id','product_orders.product_id')
        ->join('customers','customers.id','product_orders.user_id')
        ->select('product_orders.*','products.product_type','products.product_name','products.product_description','products.product_price','products.product_image','customers.full_name','customers.email','customers.phone_number')
        ->get();
        $v_order=DB::table('video_orders')->where('video_orders.user_id',$id)
        ->join('tutorials','tutorials.id','video_orders.tutorial_id')
        ->join('customers','customers.id','video_orders.user_id')
        ->select('video_orders.*','tutorials.course_name','tutorials.course_price','tutorials.category','tutorials.course_videos','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0 || $v_order->count() > 0){
             $arr = array(
                    'products_orders' => AllOrderProductsCollection::collection($order),
                    'videos_orders' => AllOrderVideoCollection::collection($v_order)
                 );
                 return   response()->json($arr, 200);

             }else{
                 
                return response(['message' => 'Order Not Found','status' =>'error']);
             }
    }
    public function store(Request $request)
    {
        $input = array(
            'product_id' => $request->product_id,
            'user_id' => $request->user_id
        );
        $rules = array(
            'product_id' => 'required',
            'user_id' => 'required'
        );
        $validator = Validator::make($input, $rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }
    	$user=Customer::find($request->user_id);
        if ($user == "" && $user == null) {
            return response()->json(["message" => "User doest not exist", "status" => 0]);
        }
        $method = Payment_method::where(['user_id' => $request->user_id])->get()->first();
        if(!($method != null || $method != "")){
            return response()->json(["message" => "Sorry! Payment method is not existing", "status" => 0]);
        }
        $products = Product::where('id', $request->product_id)->get()->first();
        if ($method->avalable_amount >=   $products->product_price) {
            $method->avalable_amount=$method->avalable_amount - $products->product_price;
            if ($method->save()) {
                $product=Product::find($request->product_id);
                $order=new ProductOrder();
                $order->user_id=$user->id;
                $order->product_id=$product->id;
                $order->latitude=$request->latitude;
                $order->longitude=$request->longitude;
                $order->address=$request->address;
                $order->save();
                $payment=new Transaction();
                $payment->charge_id="pro_order".time().$request->user_id.$request->product_id.$order->id;
                $payment->amount=$products->product_price;
                $payment->currency="usd";
                $payment->description="payment from deposit";
                $payment->status="succeeded";
                $payment->order_for="Product Purchase Amount";
                $payment->pro_order_id=$order->id;
                $payment->user_name=$user->full_name;
                $payment->email=$user->email;
                $payment->phone=$user->phone_no;
                $payment->t_type=1;
                $result=$payment->save();
                return response()->json(["message" => "Successfully purchased product", "status" => true,'transsaction' => $payment]);
            }
        }
        try {
           Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $response = \Stripe\Token::create(array(
              "card" => array(
                "number"    => $method->card_number,
                    "exp_month" => \Carbon\Carbon::parse($method->expire_date)->month,
                    "exp_year"  => \Carbon\Carbon::parse($method->expire_date)->year,
                    "cvc"       => $method->cvv_code,
                    "name"      => $method->card_holder_number
            ))); 
        } catch (\Exception $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
         }
        
        
         $token = $response->id;
         $ss = false;
         if ($method->avalable_amount > 0) {
             $ss = true;
         }
         try {
            $stripe = Stripe\Charge::create ([
                    "amount" => ($products->product_price-$method->avalable_amount)*100,
                    "currency" => "usd",
                    "source" => $token,
                    "description" => "Client from Card payment." 
            ]);
             
         } catch (\Exception $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
         }
        if ($ss = true) {
            $method->avalable_amount=0;
            $method->save();
        }
    	$product=Product::find($request->product_id);
    	$order=new ProductOrder();
    	$order->user_id=$user->id;
    	$order->product_id=$product->id;
    	$order->latitude=$request->latitude;
    	$order->longitude=$request->longitude;
    	$order->address=$request->address;
    	$order->save();
        $payment=new Transaction();
        $payment->charge_id="pro_order".time().$request->user_id.$request->product_id.$order->id;
        $payment->amount=$stripe->amount/100;
        $payment->currency=$stripe->currency;
        $payment->description=$stripe->description;
        $payment->status=$stripe->status;
        $payment->order_for="product order Amount";
        $payment->pro_order_id=$order->id;
        $payment->user_name=$user->full_name;
        $payment->email=$user->email;
        $payment->phone=$user->phone_no;
        $payment->t_type=1;
        $result=$payment->save();
    	if($result){

    	         return response(['message' => 'Order Successfully Place',"status" => true,'transsaction' => $payment]);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Place','status' =>'error']);
    	     }


    }
    
    public function allorder()
    {
        $order=DB::table('product_orders')
        ->join('products','products.id','product_orders.product_id')
        ->join('customers','customers.id','product_orders.user_id')
        ->select('product_orders.*','products.product_type','products.product_name','products.product_description','products.product_price','products.product_image','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

    	         return  AllOrderProductsCollection::collection($order);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Found','status' =>'error']);
    	     }
    }


    
    public function pendingorder()
    {
        $order=DB::table('product_orders')->where('product_orders.status','Pending')
        ->join('products','products.id','product_orders.product_id')
        ->join('customers','customers.id','product_orders.user_id')
        ->select('product_orders.*','products.product_type','products.product_name','products.product_description','products.product_price','products.product_image','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

    	         return  AllOrderProductsCollection::collection($order);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Found','status' =>'error']);
    	     }
    }
    
    public function changeorder($id,$status)
    {
        $state = "pending";
        if ($status == 1) {
            $state = "Completed";
        }elseif ($status == 2) {
            $state = "Cancelled";
        }
        $order=ProductOrder::find($id);
        $order->status=$state;
        $order->save();
        if($order){

    	         return response(['message' => 'Order Successfully Updated as '.$state,'status' =>'success']);

    	     }else{
    	         
    	        return response(['message' => 'Order Not Place','status' =>'error']);
    	     }
    }

    public function alltransaction()
    {
        $tranaction=Transaction::all();
        if($tranaction->count()>0){

                 return  AllTransactionCollection::collection($tranaction);

             }else{
                 
                return response(['message' => 'Record Not Found','status' =>'error']);
             }

    }

    public function gymnoti($id)
    {
        $noti=GymNotification::find($id);
        $notifications=DB::table('gym_notifications')->where('gym_id',$id)->get();

         if ($notifications->count() >0) {


            return  gymnotificationcollection::collection($notifications);

        }else{
            
           return response(['message' => 'Notifications Not Found','status' =>'error']);
        }
    }

    public function ProcessingOrder()
    {
        $order=DB::table('product_orders')->where('product_orders.status','Processing')
        ->join('products','products.id','product_orders.product_id')
        ->join('customers','customers.id','product_orders.user_id')
        ->select('product_orders.*','products.product_type','products.product_name','products.product_description','products.product_price','products.product_image','customers.full_name','customers.email','customers.phone_number')
        ->get();
        if($order->count()>0){

                 return  AllOrderProductsCollection::collection($order);

             }else{
                 
                return response(['message' => 'Order Not Found','status' =>'error']);
             }
    }

    public function changeorderProcessing($id)
    {
        $order=ProductOrder::find($id);
        $order->status="Processing";
        $order->save();
        if($order){

                 return response(['message' => 'Order Successfully Updated','status' =>'success']);

             }else{
                 
                return response(['message' => 'Order Not Place','status' =>'error']);
             }
    }
}
