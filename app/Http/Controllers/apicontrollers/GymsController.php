<?php

namespace App\Http\Controllers\apicontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Gymtrait;
use App\Gym;
use App\Http\Resources\RecentgymsCollection;
use App\Http\Resources\GymsCollection;
use App\Http\Resources\GymDetail;
use DB;
use App\models\GymNotification;
use App\Admin;
use App\Http\Resources\GymLoginResource;
use Validator;
use App\checkin_user;
use App\Customer;
use App\Payment_method;
use App\models\Transaction;
// use App\Traits\StripeTrait;
class GymsController extends Controller
{
    // use StripeTrait;
    public function login(Request $request)
    {
        $gym = Admin::where('email', '=', $request->input('gym_email'))->first();
            if (\Hash::check($request->password, $gym->password)) {             
                  return new GymLoginResource($gym);  
            }
            return response(['message' => 'Login Not Successfull','status' =>'error']); 
    }

    public function addgym(Request $request){
        request()->validate([
            'full_name' => 'required',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required',
            'zipcode' => 'required',
            'full_name1' => 'required',
            'street_address' => 'required',
            'phoneno' => 'required',
            'gym_detail' => 'required',
            'cost_per_day' => 'required',
            'isDetailForward' => 'required',
        ]);

        $find=Admin::where('email',$request->email)->first();
        // $find = "";

        if(!$find){

             $result=Gymtrait::senddata($request);

             if ($result == true) {

               return response(['message' => 'Gym added successfully','status' =>'success']);
            
             }else{

              return response(['message' => 'Gym Not added','status' =>'error']);
           }

        }else{

            return response(['message' => 'Gym already registered on this email','status' =>'error']);
        }

         
    }

    public function editGym(Request $request, $gym_id){
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|email|max:255',
            'zipcode' => 'required',
            'full_name1' => 'required',
            'street_address' => 'required',
            'phoneno' => 'required',
            'gym_detail' => 'required',
            'cost_per_day' => 'required',
            'isDetailForward' => 'required',
        ]);
        $find=Admin::where('id',$gym_id)->first();
        // $find = "";

        if($find){

             $result=Gymtrait::editGym($request,$gym_id);
             if ($result == true) {

               return response(['message' => 'Gym edited successfully','status' =>'success']);
            
             }else{

              return response(['message' => 'Gym Not edited','status' =>'error']);
           }

        }else{

            return response(['message' => 'Gym not found','status' =>'error']);
        }
    }

    // to check in a user
    function checkIn(Request $request, $customer_email, $gym_id){
        $count = Customer::where('email',$customer_email)->first();
        if($count != "" && $count != null ){
            if($count->count() > 0) {
                $statusCheck = checkin_user::where(['status' => 0, 'user_id' => $count->id])->get();
                if ($statusCheck->count() > 0) {
                    return response()->json(["message" => "Please Checkout all previous check-ins", "status" => 0]);
                }
                  // dd($id);
                  $check_user=new checkin_user();
                  // $check_user->gym_id=$request->gym_id;
                  $check_user->gym_id=$gym_id;
                  $check_user->user_id=$count->id;
                  $check_user->status=0;
                  $check_user->check_in=now();
                  if ($check_user->save()) {
                      return response()->json(["message" => "customer is checked in successfully", "status" => 1]);
                  }
            }
        }
            return response()->json(["message" => "No customer is found against this email"]);
    }

    // to check in a user
    function checkOut(Request $request, $customer_id, $check_id, $gym_id){
        $method = Payment_method::where('user_id' , $customer_id)->get();
            if(!($method->count() > 0)){
                return response()->json(["message" => "Sorry! Payment method is not existing", "status" => 0]);
            }
        $check_user=checkin_user::where("id", $check_id)->first();
        $gym = Admin::where('id', $gym_id)->get()->first();
        if ($gym == "" || $gym == null) {
            return response()->json(["message" => "no gym found", "status" => 0]);
        }

        
        $count = Customer::where('id',$customer_id)->first();
        if($count != "" && $count != null ){
            if($count->count() > 0) {
                // $statusCheck = checkin_user::where(['status' => 1, 'user_id' => $count->id])->get();
                // if ($statusCheck->count() > 0) {
                //     return response()->json(["message" => "Please check-out from previous sessions", "status" => 0]);
                // }
                $userCheck = checkin_user::where(['status' => 0,'user_id' => $count->id])->get();
                if (!($userCheck->count() > 0)) {
                    return response()->json(["message" => "No check-in found for this user", "status" => 5]);
                }
                  $check_user=checkin_user::where("id", $check_id)->first();
                  $check_user->status=1;
                  $check_user->check_out=now();
                  if ($check_user->update()) {
                        $diff = $check_user->updated_at->diffInMinutes($check_user->created_at);
                        $trans =$this->make_payment($request, $customer_id, $check_id, $gym_id, $diff);
                        return response()->json($trans);
                      return response()->json(["message" => "customer is checked out successfully", "status" => 1]);
                  }
    
            }
        }

            return response()->json(["message" => "No customer is found against this id"]);
    }

    function allChecksUser($user_id){
        $check_user=checkin_user::join('customers', 'customers.id', 'user_checks.user_id')->where("user_checks.user_id", $user_id)->select('user_checks.*', 'customers.email', 'customers.customer_type', 'user_checks.check_in', 'user_checks.check_out', 'user_checks.status')->get();;
        if ($check_user->count() > 0) {
           return response()->json($check_user);
        }
        else {
            return response()->json(["message" => "No record found"]);
        }
    }

    // funtion to check the checks of gyms
    function allChecks($gym_id){
        $checks = checkin_user::join('customers', 'customers.id', 'user_checks.user_id')->where("user_checks.gym_id", $gym_id)->select('user_checks.*', 'customers.email', 'customers.customer_type', 'user_checks.check_in', 'user_checks.check_out', 'user_checks.status')->get();
        if ($checks->count() > 0) {
            return response()->json($checks);
        }
        else{
            return response()->json(["message" => "No record found", "status" => 0]);
        }
    }


    // recents gyms

     public function recent(){

          $gyms=Gymtrait::latestgymes();

          if ($gyms->count() >0) {


             return  RecentgymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     }

    // Approved gyms

     public function approve($id){

          $gym=Gymtrait::changestatus($id);
          $gymnoti=new GymNotification();
            $gymnoti->message="Your Gym Approved Successfully";
              $gymnoti->gym_id=$id;
              //$gymnoti->user_id=$request->customer_id;
              $gymnoti->save();
          if ($gym) {

             return response(['message' => 'Gym Approved','status' =>'success']);

         }else{
             
            return response(['message' => 'Gym Not Approved','status' =>'error']);
         }
     }

    // Approved gyms

     public function delete($id){

          $gym=Gymtrait::deletegym($id);

          if ($gym) {

             return response(['message' => 'Gym deleted','status' =>'success']);

         }else{
             
            return response(['message' => 'Gym Not deleted','status' =>'error']);
         }
     }


         // See all gyms

     public function seeall(){

         $gyms=Admin::where('role','gym_manager')->get();
              if ($gyms->count() >0) {


                 return  RecentgymsCollection::collection($gyms);

             }else{
                 
                return response(['message' => 'Gyms Not Found','status' =>'error']);
             }
     }


    // gym Detail

     public function show($id){

             $gym=Admin::find($id);

return $gym;
              if ($gym){


                 return  new GymDetail($gym);

             }else{
                 
                return response(['message' => 'Gym Not Found','status' =>'error']);
             }
     }



    // New gyms

     public function newgyms(){

          $gyms=Gymtrait::gymslist(null);

          if ($gyms->count() >0) {


             return  GymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     }

    // Existing gyms
     public function existinggyms(){

          $gyms=Gymtrait::gymslist('Approved');

          if ($gyms->count() >0) {


             return  GymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     } 

     public function findgym($name)
     {
            $gyms=Admin::where('name', 'LIKE', "%{$name}%") 
             ->orWhere('street_address', 'LIKE', "%{$name}%")->where('role','gym_manager')
            ->get();
            if ($gyms->count() >0) {


             return  GymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     }

     public function allgyms()
     {
         $gyms=Admin::where('role','gym_manager')->get();
         if ($gyms->count() >0) {


             return  GymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     }
     public function gympaginate($number)
     {
         $gyms=Admin::where('role','gym_manager')->paginate($number);
         if ($gyms->count() >0) {


             return  GymsCollection::collection($gyms);

         }else{
             
            return response(['message' => 'Gyms Not Found','status' =>'error']);
         }
     }

     public function make_payment($request, $customer_id, $check_id, $gym_id, $diff){

            $user=Customer::find($customer_id);
            $method = Payment_method::where('user_id' , $customer_id)->get()->first();
            
            $gym = Admin::where('id', $gym_id)->get()->first();
            if ($method->avalable_amount >=   (((int)($diff/60))*(int)($gym->cost_per_day/24))) {
                $method->avalable_amount=$method->avalable_amount - (((int)($diff/60))*(int)($gym->cost_per_day/24));
                if ($method->save()) {
                    // $product=packages::find($check_id);
                    // $id = $customer_id;
                    // $package_user=new packages_user();
                    // $package_user->package_id=$check_id;
                    // $package_user->user_id=$id;
                    // $package_user->save();
                    $payment=new Transaction();
                    $payment->charge_id="chk_order".time().$customer_id.$check_id.$gym_id;
                    $payment->amount=(((int)($diff/60))*(int)($gym->cost_per_day/24));
                    $payment->currency="usd";
                    $payment->description="payment from checout for gym";
                    $payment->status="succeeded";
                    $payment->order_for="payment from checout for gym";
                    $payment->chk_id=$check_id;
                    $payment->user_name=$user->full_name;
                    $payment->email=$user->email;
                    $payment->phone=$user->phone_no;
                    $payment->t_type=1;
                    $result=$payment->save();
                    return response()->json(["message" => "Client from Card payment for gym check out", "status" => true,'transsaction' => $payment]);
                }
            }
            try {
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                 $response = \Stripe\Token::create(array(
                   "card" => array(
                     "number"    => $method->card_number,
                         "exp_month" => \Carbon\Carbon::parse($method->expire_date)->month,
                         "exp_year"  => \Carbon\Carbon::parse($method->expire_date)->year,
                         "cvc"       => $method->cvv_code,
                         "name"      => $method->card_holder_number
                 )));
            } catch (\Exception $e) {
             $body = $e->getJsonBody();
             $err  = $body['error'];
             return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
          }

             $token = $response->id;
             $ss = false;
             if ($method->avalable_amount > 0) {
                 $ss = true;
             }
            try {
               $stripe = \Stripe\Charge::create ([
                       "amount" => ((((int)($diff/60))*(int)($gym->cost_per_day/24))-$method->avalable_amount)*100,
                       "currency" => "usd",
                       "source" => $token,
                       "description" => "Client from Card payment for gym check out." 
               ]); 
            } 
            catch (\Exception $e) {
             $body = $e->getJsonBody();
             $err  = $body['error'];
             return response()->json(["detail" => $err,"message" => $e->getMessage(), "errCode" => 401],401);
          }
            if ($ss = true) {
                $method->avalable_amount=0;
                $method->save();
            }
            $payment=new Transaction();
            $payment->charge_id="pkg_order".time().$customer_id.$check_id.$gym_id;
            $payment->amount=(((int)($diff/60))*(int)($gym->cost_per_day/24));
            $payment->currency="usd";
            $payment->description="payment from checout for gym";
            $payment->status="succeeded";
            $payment->order_for="payment from checout for gym";
            $payment->chk_id=$check_id;
            $payment->user_name=$user->full_name;
            $payment->email=$user->email;
            $payment->phone=$user->phone_no;
            $payment->t_type=1;
            $result=$payment->save();
            if($result){

                     return response(['message' => 'checkout Successfully made',"status" => true,'transsaction' => $payment]);

                 }else{
                     
                    return response(['message' => 'checkout Not made','status' =>'error']);
                 }
    }


    
}
