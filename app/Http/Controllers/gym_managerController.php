<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class gym_managerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('gym_manager.layout.home');
    }
    /**
     * Display a listing of the notification.
     *
     * @return \Illuminate\Http\Response
     */
    public function notification()
    {
        return view('gym_manager.layout.notification');
    }
    /**
     * Display a listing of the inbox_screen.
     *
     * @return \Illuminate\Http\Response
     */
    public function inbox_screen()
    {
        return view('gym_manager.layout.inbox_screen');
    }
    /**
     * Display a listing of the add_new_location.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_new_location()
    {
        return view('gym_manager.layout.add_new_location');
    }
    /**
     * Display a listing of the add_new_location.
     *
     * @return \Illuminate\Http\Response
     */
    public function features()
    {
        return view('gym_manager.layout.features');
    }
     /**
     * Display a listing of the manage_users.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_users()
    {
        return view('gym_manager.layout.manage_users');
    }
     /**
     * Display a listing of the manage_users_chk_out.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage_users_chk_out()
    {
        return view('gym_manager.layout.manage_users_chk_out');
    }

 /**
     * Display a listing of the existing_employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function existing_users()
    {
        return view('gym_manager.layout.existing_users');
    }
    /**
     * Display a listing of the add_payment_method.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_payment_method()
    {
        return view('gym_manager.layout.add_payment_method');
    }

 /**
     * Display a listing of the contact_zurvos.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact_zurvos()
    {
        return view('gym_manager.layout.contact_zurvos');
    }
    /**
     * Display a listing of the add_new_user.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_new_user()
    {
        return view('gym_manager.layout.add_new_user');
    }
       /**
     * Display a listing of the my_profilee.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_profilee()
    {
        return view('gym_manager.layout.my_profile');
    }
     /**
     * Display a listing of the transaction_store.
     *
     * @return \Illuminate\Http\Response
     */
    public function transaction_store()
    {
        return view('gym_manager.layout.transaction_store');
    }
      /**
     * Display a listing of the finincial_dashborad.
     *
     * @return \Illuminate\Http\Response
     */
    public function finincial_dashborad()
    {
        return view('gym_manager.layout.finincial_dashborad');
    }
    
}
