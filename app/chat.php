<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chat extends Model
{
    protected $fillable = [
        'sender_id',
        'receiver_id', 
        'message', 
    ];

    protected $guarded = [];

    // function to propogate the child categories
    public function messages()
    {
        return $this->hasMany(chat::class, 'receiver_id', 'sender_id');
    }

    protected $table = 'chats';
}
