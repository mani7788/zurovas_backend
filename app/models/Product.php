<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function product_order()
    {
        return $this->hasMany(ProductOrder::class,'id','product_id');
    }
}
