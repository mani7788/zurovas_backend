<?php

namespace App\models;

use App\Customer;
use Illuminate\Database\Eloquent\Model;

class BuddyWorkout extends Model
{
    public function customer()
    {
        return $this->belongsTo(Customer::class , 'customer_id' , 'id');
    }
    public function Influence_buddy_workout()
    {
        return $this->belongsTo(Customer::class , 'customer_id');
    }
    public function invi_buddy_workout()
    {
        return $this->belongsTo(Customer::class , 'buddy_id');
    }
    public function workouts()
    {
        return $this->belongsTo(workout::class , 'workout_id' , 'id');
    }

    public function buddy()
    {
        return $this->belongsTo(Customer::class , 'buddy_id' , 'id');
    }
}
