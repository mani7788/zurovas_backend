<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
