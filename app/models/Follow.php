<?php

namespace App\models;
 use App\Customer;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public function influencer_followers()
    {
        return $this->belongsTo(Customer::class,'user_id','id');
    }
    public function influencer_following()
    {
        return $this->belongsTo(Customer::class,'follow_id','id');
    }
}
