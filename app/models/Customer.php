<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getUserImageAttribute($value)
    {
        if($value != null)
        {
            return url('/public/userimage/'.$value);
        }
    }
    public function posts()
    {
        return $this->hasMany(Post::class,'customer_id');
    }

    public function user_noti_customer()
    {
        return $this->belongsTo(Customer::class,'user_id');
    }
}
