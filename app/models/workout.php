<?php

namespace App\models;

use App\Influence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class workout extends Model
{
    protected $guarded = [];
    protected $table = 'workouts';
    protected $primaryKey = 'id';

    public function customer(){
        return $this->belongsTo(Customer::class , 'customer_id' , 'id');
    }
    public function influencer_budy(){
        return $this->belongsTo(Customer::class , 'customer_id');
    }

    public function buddyworkout()
    {
        return $this->hasMany(BuddyWorkout::class , 'workout_id' , 'id');
    }

    public function videos()
    {
        return $this->hasMany(WorkoutVideos::class , 'workout_id' , 'id');
    }
    public function totalvideo($workout_id){
        $id=Auth::guard('influence')->user()->id;
        return WorkoutVideos::where('workout_id',$workout_id)->where('customer_id',$id)->count();
    }
}
