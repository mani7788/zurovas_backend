<?php
namespace App\Traits;
use App\models\Gym;
use App\Admin;
use App\models\Feature;
use App\models\GymNotification;
trait Gymtrait
{

    //add new gym

    static public function senddata($request){
            $isFWD = false;
            if ($request->isDetailForward == true) {
                $isFWD = true;
            }
	    	$gym=new Admin();
	        $gym->name = $request->full_name;
	        $gym->email = $request->email;       
	        $gym->password = bcrypt($request->password);
	        $gym->full_name1 = $request->full_name1;
	        $gym->zipcode = $request->zipcode;
	        $gym->street_address = $request->street_address;
	        $gym->phoneno = $request->phoneno;
            $gym->gym_detail = $request->gym_detail;
            $gym->role = "gym_manager";
	        $gym->cost_per_day = $request->cost_per_day;
	        if($request->hasFile('gym_image')){
    	          $extension=$request->gym_image->extension();
    	          $filename=time()."_.".$extension;
    	          $request->gym_image->move(public_path('gymimage'),$filename);
    	          $gym->image=$filename;
    	        }
	        $result=$gym->save();
            $gymnoti=new GymNotification();
            $gymnoti->message="Your Gym Add Wait For Admin Approve";
              $gymnoti->gym_id=$gym->id;
              //$gymnoti->user_id=$request->customer_id;
              $gymnoti->save();
	        $features=$request->features;
            if($features != "" && $features != null){
                foreach ($features as $fea) {
                $feat=new Feature();
                $feat->gym_id = $gym->id;
                $feat->feature = $fea;  
                $feat->save(); 
            }
            }
	        
	        
	        if ($result) {
	             
	             return true;
	        }

    }
        static public function editGym($request,$gym_id){
            $isFWD = false;
            if ($request->isDetailForward == true) {
                $isFWD = true;
            }
            $image = "";
            if($request->hasFile('gym_image')){
                  $extension=$request->gym_image->extension();
                  $filename=time()."_.".$extension;
                  $request->gym_image->move(public_path('gymimage'),$filename);
                  $image=$filename;
                }  
            $data = array(
                "name" => $request->full_name,
                "email" => $request->email,
                "full_name1" => $request->full_name1,
                "zipcode" => $request->zipcode,
                "street_address" => $request->street_address,
                "phoneno" => $request->phoneno,
                "gym_detail" => $request->gym_detail,
                "cost_per_day" => $request->cost_per_day,
                "image" =>  $image,
            );
            $result = Admin::where('id', $gym_id)->update($data);
            // $gym=new Admin();
            // $gym->name = $request->full_name;
            // $gym->email = $request->email;       
            // // $gym->password = bcrypt($request->password);
            // $gym->full_name1 = $request->full_name1;
            // $gym->zipcode = $request->zipcode;
            // $gym->street_address = $request->street_address;
            // $gym->phoneno = $request->phoneno;
            // $gym->gym_detail = $request->gym_detail;
            // $gym->role = "gym_manager";
            // $gym->cost_per_day = $request->cost_per_day;
            // if($request->hasFile('gym_image')){
            //       $extension=$request->gym_image->extension();
            //       $filename=time()."_.".$extension;
            //       $request->gym_image->move(public_path('gymimage'),$filename);
            //       $gym->image=$filename;
            //     }
            // $result=$gym->update();
            // return $result;
            // $gymnoti=new GymNotification();
            // $gymnoti->message="Your Gym Add Wait For Admin Approve";
            //   $gymnoti->gym_id=$gym->id;
            //   //$gymnoti->user_id=$request->customer_id;
            //   $gymnoti->update();
            $features=$request->features;
            if($features != "" && $features != null){
                foreach ($features as $fea) {
                    $dataF = array("feature" => $fea);
                    Feature::where('gym_id',$gym_id)->update($dataF);  
                }
            }
            if ($result) {
                 
                 return true;
            }
        }

    static public function latestgymes(){

    	return Admin::latest()->where('status',null)->where('role', 'gym_manager')->limit(4)->get();
    }

    static public function changestatus($id){

    	return Admin::where('id',$id)->update(['status'=>'Approved']);
    }

    static public function deletegym($id){

        Feature::where('gym_id',$id)->delete();
        GymNotification::where('gym_id',$id)->delete();
    	return Admin::where('id',$id)->delete();
    }

     static public function gymslist($status){

    	return Admin::where('status',$status)->where('role', 'gym_manager')->get();
    }



}
