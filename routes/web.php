<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/','AdminAuth\LoginController@showLoginForm');
Route::get('login/facebook', 'AdminAuth\LoginController@redirectToProviderFacebook');
Route::get('login/facebook/callback', 'AdminAuth\LoginController@handleProviderCallbackFacebook');
Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
Route::post('/register', 'AdminAuth\RegisterController@register');
Route::get('/vendor' , 'VendorController@showLoginForm');
Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::get('/logout', 'AdminAuth\LoginController@logout');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
  Route::get('/home','Admin\HomeController@home');

  Route::group(['namespace'=>'Admin'],function(){
                          // Gym routes// View Gyms
    Route::get('gyms-all','GymsController@allGym')->name('gyms.index');
 // edit Gym
    Route::get('edit-gyms/{id}','GymsController@editGym')->name('gyms.edit');
    Route::put('edit-gyms/{id}','GymsController@update')->name('gyms.update');
 // delete Gym
    Route::delete('delete-gym/{id}','GymsController@destroy')->name('gyms.destroy');
    // Add Gym
    Route::get('add-gym','GymsController@addGym')->name('add-gym');
    Route::post('add-gym','GymsController@storeGym')->name('add-gym');

    Route::get('manage-users','CustomersController@manageUsers')->name('manage-users');
    Route::get('manage-users/{id}','CustomersController@show')->name('manage-users.show');
    Route::delete('manage-users/{id}','CustomersController@destroy')->name('manage-users.destroy');

//video orders
    Route::get('product-orders','OrderController@productindex')->name('product-orders');
    Route::get('video-orders','OrderController@videoindex')->name('video-orders');
    Route::delete('product-order/{id}','OrderController@productdestroy')->name('product-orders.destroy');
    Route::delete('video-order/{id}','OrderController@videodestroy')->name('video-orders.destroy');

    Route::get('transactions','TransactionController@index')->name('transactions.index');
    Route::delete('transactions/{id}','TransactionController@destroy')->name('transactions.destroy');

    Route::get('affiliate-partners','AffiliatePartnerController@viewAllOrders')
    ->name('affiliate-partners');

    // Partners Controller
    Route::get('add-partner','PartnerController@addPartner')->name('add-partner');
    Route::post('add-partner','PartnerController@storePartner')->name('add-partner');
  //Update Partners Controller
    Route::get('edit-partner/{id}','PartnerController@edit')->name('partner.edit');
    Route::put('edit-partner/{id}','PartnerController@update')->name('partner.update');
   Route::delete('delete-partner/{id}','PartnerController@update')->name('partner.destroy');

    // Upload Tutorial Controller
    Route::get('upload-tutorial-all','TutorialController@index')->name('tutorials.index');
    Route::get('upload-tutorial','TutorialController@uploadTutorial')->name('upload-tutorial');
    Route::post('upload-tutorial','TutorialController@storeTutorial')->name('upload-tutorial');
    Route::get('upload-tutorial/edit/{id}','TutorialController@edit')->name('upload-tutorial.edit');
    Route::get('upload-tutorial/{id}','TutorialController@show')->name('upload-tutorial.show');
    Route::put('upload-tutorial/{id}','TutorialController@update')->name('upload-tutorial.update');
  Route::delete('upload-tutorial/{id}','TutorialController@destroy')->name('upload-tutorial.destroy');


    // Product Controller
    Route::get('products','ProductsController@index')->name('product.index');
    Route::get('add-product','ProductsController@addProduct')->name('add-product');
    Route::post('add-product','ProductsController@storeProduct')->name('add-product');
    Route::get('product/edit/{id}','ProductsController@edit')->name('product.edit');
    Route::put('product/{id}','ProductsController@update')->name('product.update');
    Route::delete('product/{id}','ProductsController@destroy')->name('product.destroy');

    //exercise library
    Route::resource('exercise-lib','ExerciseLibraryController');

  });
});

Route::group(['prefix' => 'gym'], function () {
  Route::get('/login', 'GymAuth\LoginController@showLoginForm');
  Route::post('/login', 'GymAuth\LoginController@login');
  Route::post('/logout', 'GymAuth\LoginController@logout');

  Route::get('/register', 'GymAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'GymAuth\RegisterController@register');

  Route::post('/password/email', 'GymAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'GymAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'GymAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'GymAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'partner'], function () {
  Route::get('/login', 'PartnerAuth\LoginController@showLoginForm');
  Route::post('/login', 'PartnerAuth\LoginController@login');
  Route::post('/logout', 'PartnerAuth\LoginController@logout');

  Route::get('/register', 'PartnerAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'PartnerAuth\RegisterController@register');

  Route::post('/password/email', 'PartnerAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'PartnerAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'PartnerAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'PartnerAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'customer'], function () {
  Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('customer.login');
  Route::post('/login', 'CustomerAuth\LoginController@login');
  Route::post('/logout', 'CustomerAuth\LoginController@logout')->name('customer.logout');
  Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm')->name('customer.register');
  Route::post('/register', 'CustomerAuth\RegisterController@register');
  Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail')->name('customer.password.request');
  Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('customer.password.email');
  Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('customer.password.reset');
  Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');
});








Route::group(['prefix' => 'influence'], function () {
  Route::get('/login', 'InfluenceAuth\LoginController@showLoginForm')->name('influence.login');
  Route::post('/login', 'InfluenceAuth\LoginController@login');
  Route::post('/logout', 'InfluenceAuth\LoginController@logout')->name('influence.logout');

  Route::get('/register', 'InfluenceAuth\RegisterController@showRegistrationForm')->name('influence.register');
  Route::post('/register', 'InfluenceeController@store');
  Route::post('/address_detail','InfluenceeController@address_detail')->name('influence.address_detail');
//   Route::post('/social_media_links','InfluenceeController@social_media_links')->name('influence.social_media_links');
  Route::post('/proof_of_identity','InfluenceeController@proof_of_identity')->name('influence.proof_of_identity');
  Route::get('/address_detail_view_call', 'InfluenceeController@address_detail_view_call')->name('influence.address_detail_view_call');
  Route::get('/proof_of_identity_view_call', 'InfluenceeController@proof_of_identity_view_call')->name('influence.proof_of_identity_view_call');
  Route::post('/proof_of_identity_save','InfluenceeController@proof_of_identity_save')->name('influence.proof_of_identity_save');

  Route::post('/password/email', 'InfluenceAuth\ForgotPasswordController@sendResetLinkEmail')->name('influence.password.request');
  Route::post('/password/reset', 'InfluenceAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'InfluenceAuth\ForgotPasswordController@showLinkRequestForm')->name('influence.password.reset');
  Route::get('/password/reset/{token}','InfluenceAuth\ResetPasswordController@showResetForm');
  Route::post('/post', 'InfluenceeController@save_post')->name('influence.postss');
  Route::post('/fallllow', 'InfluenceeController@postfollow')->name('influence.fallllow');

  Route::get('/influence_home','InfluenceeController@index')->name('influence_home');
  Route::get('/search_page','InfluenceeController@search_page')->name('search_page');
  Route::post('/search_page','InfluenceeController@search_page_result')->name('search_page');
  Route::get('/notification_page','InfluenceeController@notification_page')->name('notification_page');
  Route::get('/free_video_page','InfluenceeController@free_video_page')->name('free_video_page');
  Route::get('/paid_video_page','InfluenceeController@paid_video_page')->name('paid_video_page');
  Route::get('/search_video_page_load','InfluenceeController@search_video_page_load')->name('search_video_page_load');
  Route::post('/search_video_page','InfluenceeController@search_video_page')->name('search_video_page');
  Route::get('/fitness_produsts_page','InfluenceeController@fitness_produsts_page')->name('fitness_produsts_page');
  Route::get('/zavour_store','InfluenceeController@zavour_store')->name('zavour_store');
  Route::get('/my_affiliated','InfluenceeController@my_affiliated')->name('my_affiliated');
  Route::get('/my_earning','InfluenceeController@my_earning')->name('my_earning');
  Route::get('/transaction_sto','InfluenceeController@transaction_store')->name('transaction_sto');
  Route::get('/credit_store','InfluenceeController@credit_store')->name('credit_store');
  Route::get('/affaliated_policy','InfluenceeController@affaliated_policy')->name('affaliated_policy');
  Route::get('/account_setting','InfluenceeController@account_setting')->name('account_setting');
  Route::post('/account_setting_update_profile','InfluenceeController@account_setting_update_profile')->name('account_setting_update_profile');
  Route::post('/place_order','InfluenceeController@product_order')->name('place_order');


  Route::get('/my_profilee','InfluenceeController@my_profile')->name('my_profilee');
  Route::get('/posts_page','InfluenceeController@posts_page')->name('posts_page');
  Route::get('/followers_page','InfluenceeController@followers_page')->name('followers_page');
  Route::get('/following_page','InfluenceeController@following_page')->name('following_page');
  Route::get('/my_orders','InfluenceeController@my_orders')->name('my_orders');
  Route::get('/my_orders_detail/{id}','InfluenceeController@show')->name('my_orders_detail');
  Route::get('/all_sessions','InfluenceeController@all_sessions')->name('all_sessions');
  Route::get('/add_payment_method','InfluenceeController@add_payment_method')->name('add_payment_method');
  Route::post('/add_payment_method_store','InfluenceeController@add_payment_method_store')->name('add_payment_method_store');
  Route::get('/FAQ_question','InfluenceeController@FAQ_question')->name('FAQ_question');
  Route::get('/FAQ_video','InfluenceeController@FAQ_video')->name('FAQ_video');
  Route::get('/contact_zurvos','InfluenceeController@contact_zurvos')->name('contact_zurvos');
  Route::post('/contact_zurvos_store','InfluenceeController@contact_zurvos_store')->name('contact_zurvos_store');
  Route::get('/bug_report','InfluenceeController@bug_report')->name('bug_report');
  Route::post('/bug_report_store','InfluenceeController@bug_report_store')->name('bug_report_store');
  Route::get('/privacy_policy','InfluenceeController@privacy_policy')->name('privacy_policy');
  Route::get('/about_zurvos','InfluenceeController@about_zurvos')->name('about_zurvos');
//   Route::get('additional_information','InfluenceeController@additional_information')->name('additional_information');

  Route::delete('/delete_order/{id}','InfluenceeController@destroy')->name('delete_order');
  Route::get('my_resources','InfluenceeController@my_resources')->name('my_resources');
  Route::get('my_resources_create','InfluenceeController@my_resources_create')->name('my_resources_create');
  Route::get('generate_affiliated_link','InfluenceeController@generate_affiliated_link')->name('generate_affiliated_link');
  Route::post('my_resources_store','InfluenceeController@my_resources_store')->name('my_resources_store');
  Route::delete('/my_resources_store/{id}','InfluenceeController@my_resources_delete')->name('my_resources_store');
  Route::post('/search_resource_page','InfluenceeController@search_resource_page')->name('search_resource_page');
  Route::post('/affiliated_link_save','InfluenceeController@affiliated_link_save')->name('affiliated_link_save');





  Route::get('workoutbuddy','InfluenceeController@workoutbuddy')->name('workoutbuddy_influ');
  Route::post('workoutbuddy_find','InfluenceeController@workoutbuddy_find')->name('workoutbuddy_find');
  Route::post('workoutbuddy_save','InfluenceeController@workoutbuddy_save')->name('workoutbuddy_save');




  Route::get('workoutlist','InfluenceeController@workoutlist')->name('workoutlist_influ');
  Route::get('message','InfluenceeController@message')->name('message');

  Route::get('invitation','InfluenceeController@invitation')->name('invitation');
  Route::post('accept_invite/{id}','InfluenceeController@accept_invite')->name('accept_invite');
  Route::post('reject_invite/{id}','InfluenceeController@reject_invite')->name('reject_invite');


  Route::get('feedback','InfluenceeController@feedback')->name('feedback');
  Route::post('feedback_storeee','InfluenceeController@feedback_storeee')->name('feedback_storeee');

  Route::get('buddylist','InfluenceeController@buddylist')->name('buddylist');



  Route::get('buildworkout','InfluenceeController@buildworkout')->name('buildworkout');
  Route::post('buildworkout_store','InfluenceeController@buildworkout_store')->name('buildworkout_store');

  Route::get('exercise','InfluenceeController@exercise')->name('exercise');
  Route::post('exercise_store','InfluenceeController@exercise_store')->name('exercise_store');
  Route::get('workout_videos/{id}','InfluenceeController@workout_exercise_video')->name('workout_videos');

  Route::post('order_workout_vedio','InfluenceeController@order_workout_vedio')->name('order_workout_vedio');



  Route::post('save_likes','InfluenceeController@save_likes')->name('save_likes');
  Route::post('save_comments','InfluenceeController@save_comments')->name('save_comments');
  Route::post('save_shares','InfluenceeController@save_shares')->name('save_shares');


  Route::delete('/unfolloww/{id}','InfluenceeController@unfolloww')->name('unfolloww');
  Route::post('/followw/{id}','InfluenceeController@followw')->name('followw');
});








Route::group(['prefix' => 'employee'], function () {
  Route::get('/login', 'EmployeeAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'EmployeeAuth\LoginController@login');
  Route::post('/logout', 'EmployeeAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'EmployeeAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'EmployeeAuth\RegisterController@register');

  Route::post('/password/email', 'EmployeeAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'EmployeeAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'EmployeeAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'EmployeeAuth\ResetPasswordController@showResetForm');
});



Route::group(['namespace'=>'Customer','prefix'=>'customer'],function(){

Route::resource('posts','PostController');
Route::get('customer-all-allgym','GymController@index')->name('customer.all.allgym');
Route::get('user-transaction','TransactionController@index')->name('user-transaction');
Route::get('customer-product','ProductController@index')->name('customer-product');
Route::get('policy','PolicyController@index')->name('policy');

});
Route::group(['prefix' => 'gym_manager'], function () {
    Route::get('home','gym_managerController@index')->name('home');
    Route::get('notification','gym_managerController@notification')->name('notification');
    Route::get('inbox_screen','gym_managerController@inbox_screen')->name('inbox_screen');
    Route::get('add_new_location','gym_managerController@add_new_location')->name('add_new_location');
    Route::get('features','gym_managerController@features')->name('features');
    Route::get('manage_users','gym_managerController@manage_users')->name('manage_users');
    Route::get('manage_users_chk_out','gym_managerController@manage_users_chk_out')->name('manage_users_chk_out');
    Route::get('add_payment_method','gym_managerController@add_payment_method')->name('add_payment_method');
    Route::get('contact_zurvos','gym_managerController@contact_zurvos')->name('contact_zurvos');
    Route::get('add_new_user','gym_managerController@add_new_user')->name('add_new_user');
    Route::get('existing_users','gym_managerController@existing_users')->name('existing_users');
    Route::get('my_profilee','gym_managerController@my_profilee')->name('my_profilee');
    Route::get('transaction_store','gym_managerController@transaction_store')->name('transaction_store');
    Route::get('finincial_dashborad','gym_managerController@finincial_dashborad')->name('finincial_dashborad');
  });
