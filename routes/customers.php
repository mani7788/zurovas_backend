    Route::post('admin-login','AdminController@adminlogin');
    // ---------------------------------------------------------------
    //All Partner
    Route::get('partner','PartnersController@index');
    //Add Partner
    Route::post('partner','PartnersController@store');
    //Show Single Profile Partner
    Route::get('partner/{id}','PartnersController@show');
    //Update Partner
    Route::post('partner/{id}','PartnersController@update');
    //Delete Partner
    Route::delete('partner/{id}','PartnersController@destroy');
    // ----------------------------------------------------------------
    //All employee
    Route::get('all-employee','EmployeeController@show');
    //add Employee
    Route::post('add-employee','EmployeeController@addemployee');
    //Edit Employee
    Route::post('edit-employee/{id}','EmployeeController@editemployee');
    // ----------------------------------------------------------------
    // Add Gym
    Route::post('add-gym','GymsController@addgym');
    // Route::post('gym-login','GymsController@login');
    // Recents Gym
    Route::get('recents-gym','GymsController@recent');
    // Recents Gym
    Route::get('approve-gym/{id}','GymsController@approve');
    // Delete Recent Gym
    Route::get('delete-gym/{id}','GymsController@delete');
    // See all Gyms
    Route::get('seeall-gyms','GymsController@seeall');
    // Gym Detail
    Route::get('gym-detail/{id}','GymsController@show');
    // New Gyms
    Route::get('new-gyms','GymsController@newgyms');
    //find gym
    Route::get('find-gym/{name}','GymsController@findgym');
    Route::get('all-gyms','GymsController@allgyms');
    //gym with paginate
    Route::get('all-gyms/{number}','GymsController@gympaginate');
    // Existing Gyms
    Route::get('existing-gyms','GymsController@existinggyms');
    // ----------------------------------------------------------------

// ====================================================================================

// ====================================================================================
    Route::POST('add_payment_method', 'StripePaymentController@add_payment_method');
    Route::get('my_orders_list/{id}','ProductOrderController@V_proallorder');
// =====================================================================================
// ===================================================================================== 
    Route::post('stripe-product-order','StripePaymentController@stripePostProduct');

    Route::post('stripe-video-order','StripePaymentController@stripePostvideo');

    Route::post('stripe-deposit-amount','StripePaymentController@stripedeposit');

    Route::get('all-transaction','ProductOrderController@alltransaction');
//======================================================================================= 
// =====================================================================================

    Route::post('product-order','ProductOrderController@store');

    Route::get('all-product-order','ProductOrderController@allorder');
    Route::get('change-product-order/{id}','ProductOrderController@changeorder');
    Route::get('pending-product-order','ProductOrderController@pendingorder');
    
    Route::POST('change-product-order-processing/{id}','ProductOrderController@changeorderProcessing');
    Route::POST('change-video-order-processing/{id}','VideoOrderController@changeorderProcessing');
    Route::get('VideoProcessingOrder','VideoOrderController@ProcessingOrder');
    Route::get('ProductProcessingOrder','ProductOrderController@ProcessingOrder');

    Route::post('video-order','VideoOrderController@store');
    Route::get('all-video-order','VideoOrderController@allorder');
    Route::get('pending-video-order','VideoOrderController@pendingorder');
    Route::get('change-video-order/{id}','VideoOrderController@changeorder');
// =====================================================================================
// =====================================================================================
    Route::get('influenceGeneratelink','InfluenceController@influenceGeneratelink');
    Route::get('influenceResources','InfluenceController@influenceResources');
    Route::get('influenceResources_page/{search}','InfluenceController@influenceResources_page');
    Route::get('influenceAffaliated_policy','InfluenceController@influenceAffaliated_policy');
// =====================================================================================